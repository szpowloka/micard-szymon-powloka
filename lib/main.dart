import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.teal[700],
        body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              radius: 100.0,
              backgroundImage: AssetImage('images/SCN_0007.jpg'),
            ),
            Text(
              'Szymon Powloka',
              style: TextStyle(
                fontFamily: 'ZhiMangXing',
                color: Colors.white,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'THE WILDCARD',
              style: TextStyle(
                fontFamily: 'Bangers',
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 15.0,
              width: 150.0,
              child: Divider(
                color: Colors.black
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 50.0),
              child: ListTile(
                leading: Icon(Icons.phone, size: 30, color: Colors.black),
                title: Text(
                  '512 723 990',
                  style: TextStyle(
                    fontFamily: 'Sans',
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 50.0),
              child: ListTile(
                leading: Icon(Icons.email, size: 30, color: Colors.black),
                title: Text(
                  'szpowloka@gmail.com',
                  style: TextStyle(
                    fontFamily: 'Sans',
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
